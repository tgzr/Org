# Gitlab

In order to participate in TGZR's activities, you need a Gitlab account. 

### What is Gitlab

Gitlab is an online platform that combines the ability to develop, secure, and operate software. It's great, it's free and it's open-source.

But dont be afraid, this does not mean you need to code or master some development tools :)
Gitlab provides a lot of collaboration tools that TGZR leverage: Email notifications, issue tracking and help desk, etc... And by creating your account you will gain access to those feature and be connected to TGZR members.

### How to create an account

Navigate to the Gitlab sign-in page: https://gitlab.com/users/sign_in

The easier way is to use an account you already have on a compatible platform:
- Google
- Github
- Twitter
- Salesforce

If you have one of those in your sleeve, click to corresponding button and follow the instructions. You'll be set up in no time.

If none of those option works for you, locate the "Don't have an account yet? Register now" below the "Sign In" button and click on "Register Now".
Fill the form prove you're not a robot (If you are a robot, please contact for specific help). Please be conscious with your password and consider activating 2 factors authentication in your profile once set up. Your security affects our security :)

### How to secure my account

[redaction needed]

### How does TGZR affect or interact with my other Gitlab projects ?

It wil just not.

TGZR Gitlab projects are all under the TGZR group and easily identified. 

Feel free to discuss on the discord server about any issue or discomfort your would encournter thogh, we're willing to make TGZR the best experience for everyone :)


