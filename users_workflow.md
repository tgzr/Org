# Users Workflows

Information and tools for managing users on gitlab and the discord server 

## New Discord User

On joining the server, the user has no role (but still reachable with `@everyone`)

### After they introduced themselves on the intoduction channel:

- Check for the nickname convention : `name(affiliation, pronons, location)`
- Ask for the Energy they want to provide, add them the corresponding discord role (`NRG-<energy>`)
- Ask for their gitlab handle (link to tuto to create on if needed) and add them to the `TGZR/Teams/Members` gitlab group.
- Add the 'Community' rôle to them.
- Post a quick message about becoming a community user:
    '''
    Welcome to the community \o/
    You can now chat in the #lobby and #lobby-fr.
    This is a great place to meet everybody and find (or create) a team to get involved :)

    Have fun !
    '''

### Some Users want to create a new Team:

- Ask for the team name, ensure it's available and valide: SFW, alpha numeric + "-" only.
- Ask for the list of discord users to include, with their gitlab handle.
- Create the gitlab team group (! not project <- this may change): `TGZR/Teams/<team-name>`
- Add gitlab handles to the group members
- Create the discord team role `Team - <team-name>` (use another team as example) and assign it to each user + assing the 'Member' role too.
- Create the discord team channel: 
    - name : `team-<team-name>`
    - type : private
    - permissions: 
        - uncheck 'See the channel' for `@everyone` 
        - add the `Team - <team-name>` role with 'See the channel' checked.
        - add the 'Admin' and 'Team - Org' role with 'See the channel' checked.
- Ask for a logo (not mandatory but would be very nice) or propose introcution to someone w/ art energy who could design it.
- In the team channel, post a quick message about becoming a Member:
    '''
    You've just joined the team <Team-name> !
    
    You are now known as "Member" of TGZR, and have access to the `Chapter` and `Modules` channels \o/

    Chapters will let you chat with your energy peers. Feel free to introduce yourself more in depth there, espetially to the Animator(s) who will be able to present the chapter activities and meeting oganisation :)

    Modules are where we create and resolve the "Goals", during chat and/or meetings.
    You team can enrole on a goal by asking to the Module Animator(s) who will then give you access to the related gitlab projects.

    This channel is yours !
    Discussions here a only accessible to you, the Org team and the server admins.
    Make it your home and have fun :)

    For any question, feel free to ping `@team-org` here or in a public channel !
    Cheers :)
    '''

### A User wants to join a Team:

- Have a chat with the team and ensure everyone is Ok.
- Add the corresponding discord role (`team-<team_name>`)
- Add them to the gitlab team group members `TGZR/Teams/(team_name>`
- In the team channel, post a quick message about becoming a Member (derive it from the message in previous section)



